const mongoose = require('mongoose')

const FIELDS = {
  name: {
    type: String,
    default: 'Nome do VENDEDOR PADRAO'
  },
  email: {
    type: String,
    default: 'Email do VENDEDOR PADRAO'
  },
  phone: {
    type: String,
    default: 'Telefone do VENDEDOR PADRAO'
  },
  // Administrador
  administrator: {
    type: String,
    default: 'Porto Seguro'
  },
  group_id: {
    type: String,
    default: '0000'
  },
  quota_id: {
    type: String,
    default: '0000'
  },
  type: {
    type: String,
    default: 'imóvel PADRAO'
  },
  // Dados da cota
  status: {
    type: String,
    default: 'STATUS DEFAULT SEM ENUM'
    // enum: ['Contemplado', 'Não contemplado', 'Cancelado']
  },
  amount_of_credit: { // valor do crédito
    type: Number,
    default: 0
  },
  amount_of_entry: { // entrada
    type: Number,
    default: 0
  },
  payment_term: { // prazo
    type: Number,
    default: 12
  },
  paid_installments: { // parcelas pagas
    type: Number,
    default: 0
  },
  amount_of_installment: { // valor da parcela
    type: Number,
    default: 0
  },
  paid_percentage: { // parcelas pagas
    type: Number,
    default: 0
  },
  balance_of_discharge: { //saldo devedor de quitação
    type: Number,
    default: 0
  },
  // Negociação
  desired_amount: { // Valor do pretendido
    type: Number,
    default: 0
  },
  message: {
    type: String,
    default: 'message PADRAO'
  },
  files: [{
    type: String,
  }],
  how_know: {
    type: String,
    default: 'Como conheceu PADRAO'
  },
  // Do outro form
  month_of_readjustment: {
    type: Number,
    default: 1
  },
  next_expiration: {
    type: Date,
    default: Date.now
  },
  next_plot: { // prox parcela
    type: Date,
    default: Date.now
  },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  owner_userID: { type: String }, // id do MYSQL
  buyer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  buyer_userID: { type: String }, // id do MYSQL

}

const SCHEMA = mongoose.Schema(FIELDS)

module.exports = SCHEMA