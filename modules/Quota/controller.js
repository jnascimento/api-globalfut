const MODEL = require('./model')

// {
//   name,
//   email,
//   phone,
//   administrator,
//   group_and_cota,
//   type,
//   status,
//   amount_of_credit,
//   amount_of_entry,
//   payment_term,
//   paid_installments,
//   amount_of_installment,
//   paid_percentage,
//   balance_of_discharge,
//   desired_amount,
//   message,
//   files,
//   how_know,
//   month_of_readjustment,
//   next_expiration,
//   next_plot
// }

const create = async (req, res) => {
  const {
    name,
    email,
    phone,
    administrator,
    group_and_cota,
    type,
    status,
    amount_of_credit,
    amount_of_entry,
    payment_term,
    paid_installments,
    amount_of_installment,
    paid_percentage,
    balance_of_discharge,
    desired_amount,
    message,
    files,
    how_know,
    month_of_readjustment,
    next_expiration,
    next_plot
  } = req.body

  const result = await MODEL.create(req.body)
  return res.json(result)
}
const list = async (req, res) => {

  const result = await MODEL.find({}).lean()
  return res.json(result)
}
const findOne = async (req, res) => {
  const { _id } = req.params
  const query = { _id }
  const result = await MODEL.findOne(query).lean()
  return res.json(result)
}
const update = async (req, res) => {

  const { _id } = req.params
  const query = { _id }
  const result = await MODEL.updateOne(query)

  return res.json(result)
}
const remove = async (req, res) => {
  const { _id } = req.params
  const query = { _id }
  const result = await MODEL.remove(query)

  return res.json(result)
}

const CONTROLLER = {
  create,
  list,
  findOne,
  update,
  remove
}

module.exports = CONTROLLER