const express = require('express')
const router = express.Router()

const Controller = require('./controller')

router.post('/', Controller.create)
router.get('/', Controller.list)
router.get('/:_id', Controller.findOne)
router.put('/:_id', Controller.update)
router.delete('/:_id', Controller.remove)

module.exports = router