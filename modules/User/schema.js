const mongoose = require('mongoose')

const TEAM_DEFAULT = '5befe69d6a348c0931950b9b'

const FIELDS = {
  type: {
    type: String,
    enum: ['player', 'manager'],
    default: 'player'
  },
  name: {
    type: String,
    default: 'Nome próprio'
  },
  email: {
    type: String,
    default: 'email@teste.com',
    index: true,
    unique: true
  },
  password: {
    type: String,
    default: '12345678'
  },
  salt: {
    type: String,
    default: '12345678'
  },
  overall: {
    type: Number,
    default: 70
  },
  console: {
    type: String,
    default: 'psn'
  },
  name_psn: {
    type: String,
    default: 'Nome na PSN',
    index: true,
    unique: true
  },
  name_xbox: {
    type: String,
    default: 'Nome na XBOX',
    index: true,
    unique: true
  },
  name_pc: {
    type: String,
    default: 'Nome na PC',
    index: true,
    unique: true
  },
  position: {
    type: String,
    enum: [
      'goleiro', 
      'lateral direito', 
      'lateral esquerdo', 
      'zagueiro',
      'volante',
      'meia ofensivo',
      'meia central',
      'meia direita',
      'meia esquerda',
      'ponta direita',
      'ponta esquerda',
      'atacante central',
      'atacante direita',
      'atacante esquerda',
    ],
    default: 'zagueiro',
    lowercase: true // Always convert `test` to lowercase
  },
  team: { 
    type: mongoose.Schema.Types.ObjectId, ref: 'Team',
    default: TEAM_DEFAULT
  },
  created_at: {
    type: Date,
    default: Date.now
  }
}

const SCHEMA = mongoose.Schema(FIELDS)

module.exports = SCHEMA