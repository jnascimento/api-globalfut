const express = require('express')
const router = express.Router()

const Controller = require('./controller')

router.post('/', Controller.create)
router.post('/login/email', Controller.loginEmail)
router.post('/login/password', Controller.login)
router.post('/login/facebook/callbacl', Controller.loginFacbookCB)
router.get('/', Controller.list)
router.get('/mercadodabola', Controller.getMercadoDaBola)
router.get('/create/crypto', Controller.createCrypto)
router.get('/exists/:field/:value', Controller.exists)
router.get('/:id', Controller.get)
router.put('/:id', Controller.update)
router.delete('/:id', Controller.remove)
router.delete('/drop', Controller.drop)

module.exports = router