const MODEL = require('./model')
const Team = require('../Team/model')


const crypto = require('crypto')

const SALT = crypto.randomBytes(32).toString('hex')
const iteration = 99999
const keylen = 128


let SESSION = {}

global.SESSION = SESSION

const genHash = (password, salt) => {

  // console.log('genHash\n\n')
  // console.log({password})
  // console.log({salt})
  const hashedSalted = crypto.pbkdf2Sync(password, salt, iteration, keylen, 'sha512')
    .toString('hex')

  // console.log({salt})
  return { salt: salt, hash: hashedSalted }
}

const valHash = (password, userHash, userSalt) => {
  const hash = crypto.pbkdf2Sync(password, userSalt, iteration, keylen, 'sha512').toString('hex')
  return hash === userHash
}


const toObject = (result, arr) => {
  return Object.assign(
    result, 
    {[arr[0]]: arr[1].replace('\r', '')}
  )
}

// console.log(
//   e.reduce(toObject, {})
// )

const create = async (req, res, next) => { 
  const data = Object.entries(req.body).reduce(toObject, {})
  // const data = req.body

  if (data.password.trim() === "") return false
  const { salt, hash } = genHash(data.password, SALT)

  data.password = hash
  data.salt = salt
  data.email = data.email.toLowerCase()
  console.log('create: ', data)

  // console.log({salt}, {hash})
  const result = await MODEL.create(data)
  console.log('result: ', result)

  const _id = result._id
  const team = "5befe69d6a348c0931950b9b" // server
  // const team = "5bef838aabd9acbd69811b83" // local


  const query = {
    _id: data.team ? data.team : team
  }

  const mod = {
    $push: {
      players: `${_id}`
    }
  }
  console.log('query: ', query)
  console.log('mod: ', mod)
  const result2 = await Team.update(query, mod)
  
  console.log('result2: ', result2)
  res.json(!!result)
}


const createCrypto = async (req, res, next) => { 
  // const data = Object.entries(req.body).reduce(toObject, {})
  // const data = req.body


  const result = await MODEL.find({})

  const players = result.map(async u => {

    const o = u
    const { salt, hash } = genHash(u.password, SALT)
    // o.password_old = o.password
    o.password = hash
    o.salt = salt


    console.log({o})
    const result = await MODEL.updateOne({_id: o._id}, o)
    console.log({result})
    return result
  })

  return res.json(players)
  // const { salt, hash } = genHash(data.password, SALT)

  // data.password = hash
  // data.salt = salt
  data.email = data.email.toLowerCase()
  console.log('create: ', data)

  // console.log({salt}, {hash})
  // const result = await MODEL.create(data)
  console.log('result: ', result)

  const _id = result._id
  const team = "5befe69d6a348c0931950b9b" // server
  // const team = "5bef838aabd9acbd69811b83" // local


  const query = {
    _id: data.team
  }

  const mod = {
    $push: {
      players: `${_id}`
    }
  }
  console.log('query: ', query)
  console.log('mod: ', mod)
  // const result2 = await Team.update(query, mod)
  
  console.log('result2: ', result2)
  res.json(!!result)
}


const loginEmail = async (req, res, next) => { 
  console.log('loginEmail')
  const data = Object.entries(req.body).reduce(toObject, {})
  // const data = req.body


  const email = data.email
  data.email = {'$regex' : `^${email}$`, '$options' : 'i'}

  delete data.password
  
  console.log('login: ', data)

  const result = await MODEL.findOne(data)

  // const { salt, has} = genHash(data.password)

  // data.password = hash

  // // console.log({salt}, {hash})

  // console.log('result: ', result)

  res.json(!!result)
}

const login = async (req, res, next) => { 
  console.log('login')
  const data = Object.entries(req.body).reduce(toObject, {})
  // const data = req.body

  const __pass = data.password
  // delete data.password
  // const email = data.email
  const q = {email: {'$regex' : data.email, '$options' : 'i'}}

  // data.password = password
  console.log('login data: ', q)
  const User = await MODEL.findOne(q)
  console.log('User: ', User)

  // console.log('__pass: ', __pass)
  // console.log('User.salt: ', User.salt)


  // const { salt, hash } = genHash(User.password, SALT)

  const isSamePaswword = valHash(__pass, User.password, User.salt)
  console.log({isSamePaswword})
  console.log({User})

  // cons _console =
  
  if (isSamePaswword) {
    
    global.SESSION[User._id] = {
      created_at: Date.now(),
      active: true,
      console: User.console
    }

    console.log('SESSION: ', global.SESSION)
    return res.json({
      _id: User._id, 
      name_psn: User.name_psn, 
      name_xbox: User.name_xbox, 
      name_pc: User.name_pc,
      team: User.team
    })
  }
  // if (User) return res.json(User._id)
  else return res.json(false)
  

  // const result = await MODEL.findOne(data)


  // data.password = hash


  // console.log('result: ', result)

  
}


const list = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = await MODEL.find({}).lean().populate('team')

  console.log('result:', result);
  res.json(result)
}

const getMercadoDaBola = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('getMercadoDaBola');
  console.log('------------------------------------');

  const query = {team: "5befe69d6a348c0931950b9b"}
  const result = await MODEL.find(query).lean()

  console.log('result:', result);
  res.json(result)
}


const get = async (req, res, next) => { 
  console.log('------------------------------------');
  console.log('get');
  console.log('------------------------------------');
  const query = { _id: req.params.id }
  console.log('query: ', query);
  const result = await MODEL.findOne(query).populate('team').lean()
  console.log('result: ', result);

  res.json(result)
}
const update = async (req, res, next) => {
  console.log("updaye")
  const query = { _id: req.params.id }
  // const data = req.body

  const data = Object.entries(req.body).reduce(toObject, {})
  console.log({query}, {data})
  const result = await MODEL.update(query, data)

  res.json(result)
}
const remove = async (req, res, next) => {
  const query = { _id: req.params.id }
  const result = await MODEL.remove(query)

  res.json(result)
}

const drop = async (req, res, next) => {
  const query = {}
  const result = await MODEL.remove(query)

  res.json(result)
}


const getQuery = (req) =>
  (req.params)
    ? ({ [req.params.field]: req.params.value })
    : ({ [req.body.field]: req.body.value })


const exists = async (req, res, next) => {
  const query = getQuery(req)
  const result = await MODEL.find(query)

  res.json(!!result.length)
}


const getSESSION = (_id) => {

  return global.SESSION[_id]
}

const loginFacbookCB = (obj) => {

  console.log("loginFacbookCB obj:", obj)
}

// const getSESSIONRoute = async (req, res, next) => {

//   return getSESSION(req
// }

const CONTROLLER = {
  create,
  list,
  get,
  update,
  remove,
  exists,
  drop,
  login,
  loginEmail,
  createCrypto,
  getMercadoDaBola,
  getSESSION,
  loginFacbookCB
}

module.exports = CONTROLLER