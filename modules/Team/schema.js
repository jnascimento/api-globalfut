const mongoose = require('mongoose')

const FIELDS = {
  name: {
    type: String,
    default: 'teste'
  },
  console: {
    type: String,
    default: 'psn'
  },
  badge: {
    type: String,
    default: 'default'
  },
  badge_exists: {
    type: Boolean,
    default: false
  },
  division: {
    type: String,
    default: 'primeira'
  },
  overall: {
    type: Number,
    default: 70
  },
  payment: {
    type: String,
    default: 'pending'
  },
  champ: { type: mongoose.Schema.Types.ObjectId, ref: 'Champ' },
  manager: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  submanager: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  players: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  candidates: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],

  // players: [{ 
  //   id: {
  //     type: mongoose.Schema.Types.ObjectId, 
  //     ref: 'User'},
  //   accepted: {
  //     type: Boolean,
  //     default: false
  //   }}
  
}

const SCHEMA = mongoose.Schema(FIELDS)

module.exports = SCHEMA