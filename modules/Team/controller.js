const MODEL = require('./model')
const MERCADO_DA_BOLA_ID = '5befe69d6a348c0931950b9b'

const CONFIG = require('../../config/team')

const UserModel = require('../User/model')
const ChampModel = require('../Champ/model')
const GameModel = require('../Game/model')
const Champ = require('../Champ/controller')

const toObject = (result, arr) => {
  return Object.assign(
    result, 
    {[arr[0]]: arr[1].replace('\r', '')}
  )
}


const validate = (config = CONFIG) => {
  const field = config.field
  
  return (data[field].length >= CONFIG[field].min && data[field].length <= CONFIG[field].max)
}
const create = async (req, res, next) => { 
  // const data = req.body

  console.log('req.body:', req.body)
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data = req.body
  console.log('create team data:', data)
  
  const result = await MODEL.create(data)
  console.log({result})

  // const team = result._id
  // const query = {
  //   _id: data.champ
  // }
  // const mod = {
  //   $push: { teams: team }
  // }
  // console.log({query})
  // console.log({mod})
  // const result2 = await Champ.__update(query, mod)
  // console.log('create team result2:', result2)
  return res.json(result)

}

const createAll = async (req, res, next) => { 

  console.log('req.body:', req.body)
  const data = Object.entries(req.body).reduce(toObject, {})
  // const data = req.body

  data.players = []
  data.players.push(data.manager)
  
  console.log('create team data:', data)
  
  const result = await MODEL.create(data)
  console.log("\n createXbox result: ", result)

  if (result) {
    const queryUser = {
      _id: data.manager
    }
    const mod = {
      team: result._id
    }
    
    const result2 = await UserModel.updateOne(queryUser, mod)
    console.log('create team atualizado no user:', result2)
  

  }
  return res.json(result)

}

const createXbox = async (req, res, next) => { 

  console.log('req.body:', req.body)
  const data = Object.entries(req.body).reduce(toObject, {})
  // const data = req.body
  data.console = 'xbox';
  data.players = []
  data.players.push(data.manager)
  
  console.log('create team data:', data)
  
  const result = await MODEL.create(data)
  console.log("\n createXbox result: ", result)

  if (result) {
    const queryUser = {
      _id: data.manager
    }
    const mod = {
      team: result._id
    }
    
    const result2 = await UserModel.updateOne(queryUser, mod)
    console.log('create team XBOX atualizado no user:', result2)
  

  }
  return res.json(result)

}

const list = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = await MODEL.find({}).populate('players manager submanager')

  console.log('result:', result);
  return res.json(result)

  // res.json(
  //   await MODEL.find({})
  // )
}


const listConsolePSN = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = await MODEL.find({console: /psn/gim}).populate('players manager submanager')

  console.log('result:', result);
  return res.json(result)

  // res.json(
  //   await MODEL.find({})
  // )
}



const listConsoleXBOX = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = await MODEL.find({console: /xbox/gim}).populate('players manager submanager')

  console.log('result:', result);
  return res.json(result)

  // res.json(
  //   await MODEL.find({})
  // )
}



const count = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = (await MODEL.find({}).lean())
                    // .map(
                    //   ({_id, name, console, division}) => 
                    //   ({ 
                    //       // _id, 
                    //       name: name.trim(), 
                    //       console, 
                    //       division })
                    // )

  console.log('result:', result);
  return res.json(result.length)

  // res.json(
  //   await MODEL.find({})
  // )
}

const listSmall = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = (await MODEL.find({}).lean())
                    .map(
                      ({_id, name, console, division}) => 
                      ({ 
                          _id, 
                          name: name.trim(), 
                          console, 
                          division })
                    )

  console.log('result:', result);
  return res.json(result)

  // res.json(
  //   await MODEL.find({})
  // )
}


const listXbox = async (req, res, next) => {
  console.log('------------------------------------');
  console.log('list');
  console.log('------------------------------------');
  const result = await MODEL.find({console: /xbox/gim}).lean()
                    // .map(
                    //   ({_id, name, console, division}) => 
                    //   ({ 
                    //       // _id, 
                    //       name: name.trim(), 
                    //       console, 
                    //       division })
                    // )

  console.log('result:', result);
  return res.json(result)

  // res.json(
  //   await MODEL.find({})
  // )
}

const get = async (req, res, next) => { 
  const query = { _id: req.params.id }
  const result = await MODEL.findOne(query).lean().populate('players manager submanager candidates')

  
  res.json(result)
}
const update = async (req, res, next) => {
  const query = { _id: req.params.id }
  const data = Object.entries(req.body).reduce(toObject, {})
  console.log({query}, {data})
  const result = await MODEL.updateOne(query, data)

  res.json(result)
}

const updateManager = async (req, res, next) => {
  const query = { _id: req.params.id }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data = req.body
  console.log({query}, {data})
  const result = await MODEL.updateOne(query, data)

  res.json(result)
}

const updateSubManager = async (req, res, next) => {
  const query = { _id: req.params.id }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data = req.body
  console.log({query}, {data})
  const result = await MODEL.updateOne(query, data)

  res.json(result)
}

const addCandidate = async (req, res, next) => {
  const query = { _id: req.params.id }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data = { $addToSet: { candidates: req.body.player_id } }
  console.log({query}, {data})
  const result = await MODEL.updateOne(query, data)

  res.json(result)
}

const removeCandidate = async (req, res, next) => {
  const query = { _id: req.params.id }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data = { $pull: { candidates: req.body.player_id } }
  console.log({query}, {data})
  const result = await MODEL.updateOne(query, data)

  res.json(result)
}

const moveCandidateToPlayer = async (req, res, next) => {

  const TEAM_ID = req.params.id

  // const queryCandidates = {candidates: {$in: [req.body.player_id]}}
  // const resultCandidates = (await MODEL.find(queryCandidates)).map(t => String(t._id)).filter(_id => _id !== TEAM_ID)
  // console.log({queryCandidates}, {resultCandidates}, {TEAM_ID})

  // const queryRemoveTeams = {
  //   _id: {$in: resultCandidates}
  // }

  // const result = await MODEL.find(queryCandidates)
  

  const queryCandidates = {candidates: {$in: [req.body.player_id]}}
  const dataCandidates = { 
    $pull: { candidates: req.body.player_id }
  }
  const resultCandidates = await MODEL.updateMany(queryCandidates, dataCandidates)
  console.log({queryCandidates}, {dataCandidates}, {resultCandidates})

  // const data = { 
  //   $addToSet: { players: req.body.player_id }, 
  //   $pull: { candidates: req.body.player_id }
  // }

  const query = { _id: req.params.id }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data = { 
    $addToSet: { players: req.body.player_id }, 
    $pull: { candidates: req.body.player_id }
  }
  
  console.log({query}, {data})
  const result = await MODEL.updateOne(query, data)


  const query2 = { _id: MERCADO_DA_BOLA_ID }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data2 = { 
    // $addToSet: { players: req.body.player_id }, 
    $pull: { players: req.body.player_id }
    
  }

  const result2 = await MODEL.updateOne(query2, data2)

  const query3 = {_id: req.body.player_id}
  const data3 = {$set: { team: req.params.id }}
  const result3 = await UserModel.updateOne(query3, data3)
  console.log({result}, {result2}, {result3})
  res.json(result)
}


const removePlayer = async (req, res, next) => {
  const query = { _id: req.params.id }
  const data = Object.entries(req.body).reduce(toObject, {})

  const mod = {
    $pull: {
      players: req.params.id_player
    }
  }
  console.log({query}, {data}, {mod})
  const result = await MODEL.updateOne(query, mod)
  console.log({result})

  const query2 = { _id: MERCADO_DA_BOLA_ID }
  // const data = Object.entries(req.body).reduce(toObject, {})
  const data2 = { 
    $addToSet: { players: req.body.player_id }
    // $pull: { candidates: req.body.player_id }
  }
  const result2 = await MODEL.updateOne(query, mod)


  const query3 = {_id: req.params.id_player}
  const data3 = {
    $set: {
      team: MERCADO_DA_BOLA_ID
    }
  }
  const result3 = await UserModel.updateOne(query3, data3)
  console.log({result}, {result2}, {result3})

  res.json(result)
}

const remove = async (req, res, next) => {
  const query = { _id: req.params.id }
  const result = await MODEL.remove(query)

  res.json(result)
}

const getQuery = (req) =>
  (req.params)
    ? ({ [req.params.field]: req.params.value })
    : ({ [req.body.field]: req.body.value })


const getChampPontosEJogos = async (req, res, next) => {
  const query = { _id: req.params.id}
  const pontosEJogos = await MODEL.find({result_teamA: { $exists: true}})

  console.log({pontosEJogos})
}
const exists = async (req, res, next) => {
  const query = getQuery(req)
  const result = await MODEL.find(query)

  res.json(!!result.length)
}
const drop = async (req, res, next) => {
  const query = {}
  const result = await MODEL.remove(query)

  res.json(result)
}

const divisionsIDs = [
  '5bde3067d9377116964dc49f',
  '5bde3068d9377116964dc4a0',
  '5bde3068d9377116964dc4a1',
  '5bde3068d9377116964dc4a2',
  '5bde3068d9377116964dc4a3'
]

const divisions = [
  'primeira',
  'segunda',
  'terceira',
  'quarta',
  'quinta'
]

const divisionsName = [
  '1º Global Division',
  '2º Global Division',
  '3º Global Division',
  '4º Global Division',
  '5º Global Division',
]


const generateNames = (size = 20) => {
  const alpha = [...Array(26)]
    .map( _=> (++i).toString(36),i=9)
    .slice(0, size)
    .map(s => 'TIME ' + s.toUpperCase())

  return alpha
}


const generateNamesCopa = (size = 36) => {
  const alpha = [...Array(size)]
    .map( _=> (++i).toString(36),i=9)
    .slice(0, size)
    .map(s => 'TIME ' + s.toUpperCase())

  return alpha
}

const corrigeNomes = async () => {

  const names = generateNames()
  // const query = {champ: "5bcfe1de4cbd850015d80e2b" }

  // divisionsName.forEach(async (champ, c) => {
  //   // const query = {_id: divisions[c]}
  //   // console.log({query})

  //   try {
      
  //     const data = {

  //     }

  //     // const _champ = await ChampModel.findOne(query)
  //     // const mod = {
  //     //   $set: {
  //     //     players: []
  //     //   }
  //     // }
  //     // const result = await ChampModel.updateOne(query, mod)
  //     console.log({_champ})
  //     // const champ = {
  //     //   name: champ,
  //     //   division: 
  //     // }
  //   } catch (error) {
  //     console.log('ERRORRR: ', {error})
  //   }
  // })
  names.forEach(async (name, i) => {

    const data = {
      name,
      division: divisions[4]
    }

    console.log({data})
    const result = await MODEL.create(data)
    console.log({result})

    const team = result._id

    const query = {
      _id: divisionsIDs[4]
    }

    const mod = {
      $push: { teams: team }
    } 

    console.log({mod})

    const result2 = await Champ.__update(query, mod)
    console.log('create team result2:', result2)

    return result2
  })
}



const createCopa = async (req, res, next) => {
  
  const _id = req.params.id ? req.params.id : '5c0b6286742a321a1ffab836'

  const length = 32
  const lengthGroup = 4
  const TEAMS = Array.from({length}, (v, k) => 
    ({
      name: "TEAM" + (k + 1),
      champ: _id
    }))

  const resultT = await MODEL.create(TEAMS)

  return res.json(resultT)
  const groups = length / lengthGroup

  // const TEAMSOBJ = TEAMS.reduce(
  //   (obj, team) => ({...obj, [team]: {
  //     vitorias: 0,
  //     empates: 0,
  //     derrotas: 0,
  //   }}), {})

  console.log(TEAMS)
  // console.log(TEAMSOBJ)

  const toChunk = (length, r = []) => (acc, cur, i, self) => {
    r = [...r, cur]
    if (!((i + 1) % length) || i === self.length - 1) {
      acc = [...acc, r]
      r = []
    }
    return acc
  }

  const chunk = (list, length) => 
    list.reduce(toChunk(length), [])

  const getGroups = (teams, groups) => {

    return chunk(teams, lengthGroup)
  }

  const groupsWithTeams = getGroups(TEAMS, groups)

  // console.log({groupsWithTeams})

  const toGames = (positions) => (result, arr, i, teams) => {
    console.log({positions})
    console.log({teams})
    const game = positions
      .map(pos => pos.map(p => [teams[p[0]], teams[p[1]]]))
    return [...result, game]
  }

  const positions = [
    [[0, 1], [2, 3]],
    [[0, 2], [1, 3]],
    [[0, 3], [1, 2]]
  ]
  const getGames = (groups) => 
    groups.map((group, i) => 
      positions.map(round => 
        [
          [group[round[0][0]], group[round[0][1]]],
          [group[round[1][0]], group[round[1][1]]]
        ]
      )

    )

  // console.log({groupsWithTeams})
  // const groupGames = getGames(groupsWithTeams)
  // console.log('groupGames: ', JSON.stringify(groupGames))

  res.json(TEAMS)
}

let final = []

const toGames = (champ) => (result, prev, i, arr) => {
  if ( i === arr.length - 1)
    return result

  const games = arr.slice(i+1).map((t, c) => {
    // [prev, t]

    const mod = {
      teamA: `${prev._id}`,
      teamB: `${t._id}`,
      champ
    }
    console.log('mod: ', mod)


    return mod
  })

  console.log('games: ', games.length)
  final = [...final, ...games]

  // console.log({final})
  console.log('length: ', final.length)
  return final
}

const getGames = (teams, _id) => teams.reduce(toGames(_id), [])

const flatten = (arr) => {
  return [].concat(
    ...arr.map(x => Array.isArray(x) ? flatten(x) : x)
  )
}

const a = [
  [[11, 1], [12, 2], [13, 3], [14, 4], [15, 5], [16, 6], [17, 7], [18, 8], [19, 9], [20, 10]],
  [[1, 12], [11, 13], [2, 14], [3, 15], [4, 16], [5, 17], [6, 18], [7, 19], [8, 20], [9, 10]],
  [[13, 1], [14, 12], [15, 11], [16, 2], [17, 3], [18, 4], [19, 5], [20, 6], [10, 7], [9, 8]],
  [[1, 14], [13, 15], [12, 16], [11, 17], [2, 18], [3, 19], [4, 20], [5, 10], [6, 9], [7, 8]],
  [[15, 1], [16, 14], [17, 13], [18, 12], [19, 11], [20, 2], [10, 3], [9, 4], [8, 5], [7, 6]],
  [[1, 16], [15, 17], [14, 18], [13, 19], [12, 20], [11, 10], [2, 9], [3, 8], [4, 7], [5, 6]],
  [[17, 1], [18, 16], [19, 15], [20, 14], [10, 13], [9, 12], [8, 11], [7, 2], [6, 3], [5, 4]],
  [[1, 18], [17, 19], [16, 20], [15, 10], [14, 9], [13, 8], [12, 7], [11, 6], [2, 5], [3, 4]],
  [[19, 1], [20, 18], [10, 17], [9, 16], [8, 15], [7, 14], [6, 13], [5, 12], [4, 11], [3, 2]],
  [[1, 20], [19, 10], [18, 9], [17, 8], [16, 7], [15, 6], [14, 5], [13, 4], [12, 3], [11, 2]],
  [[10, 1], [9, 20], [8, 19], [7, 18], [6, 17], [5, 16], [4, 15], [3, 14], [2, 13], [11, 12]],
  [[1, 9], [10, 8], [20, 7], [19, 6], [18, 5], [17, 4], [16, 3], [15, 2], [14, 11], [13, 12]],
  [[8, 1], [7, 9], [6, 10], [5, 20], [4, 19], [3, 18], [2, 17], [11, 16], [12, 15], [13, 14]],
  [[1, 7], [8, 6], [9, 5], [10, 4], [20, 3], [19, 2], [18, 11], [17, 12], [16, 13], [15, 14]],
  [[6, 1], [5, 7], [4, 8], [3, 9], [2, 10], [11, 20], [12, 19], [13, 18], [14, 17], [15, 16]],
  [[1, 5], [6, 4], [7, 3], [8, 2], [9, 11], [10, 12], [20, 13], [19, 14], [18, 15], [17, 16]],
  [[4, 1], [3, 5], [2, 6], [11, 7], [12, 8], [13, 9], [14, 10], [15, 20], [16, 19], [17, 18]],
  [[1, 3], [4, 2], [5, 11], [6, 12], [7, 13], [8, 14], [9, 15], [10, 16], [20, 17], [19, 18]],
  [[2, 1], [11, 3], [12, 4], [13, 5], [14, 6], [15, 7], [16, 8], [17, 9], [18, 10], [19, 20]]

]


const toGame = (teams) => (game) => {
  // console.log({game})
  if (game){
    const r = game.map((g) => {
      // console.log({g})
      const o = [teams[ g[0]-1], teams[ g[1]-1] ]
      // console.log({o})

      return o
    })
    return r
  }
}

const getGames2 = (teams) => {

  const games = a.filter(e => e).map(toGame(teams))

  return games
}

const createGames = async (champ = '5bde3068d9377116964dc4a1') => {

  
  const query = {_id: champ}
  console.log({query})
  const Champ = await ChampModel.findOne(query).lean().populate('teams')
  console.log('Times do campeonato: ', Champ.teams.length)

  const games = getGames2(Champ.teams)
  // console.log({games})

  const data = {
    $set: {sequence: games}
  }
  // const 

  const r = await ChampModel.updateOne(query, data)
  console.log('r: ', r)

  return r
}

// ;(async() => {

//   const res = await createGames()

//   console.log(res)

// })()

const CONTROLLER = {
  create,
  createAll,
  createXbox,
  count,
  list,
  listSmall,
  listXbox,
  listConsolePSN,
  listConsoleXBOX,
  get,
  update,
  updateManager,
  updateSubManager,
  remove,
  exists,
  drop,
  getChampPontosEJogos,
  removePlayer,
  createCopa,
  addCandidate,
  removeCandidate,
  moveCandidateToPlayer
}

module.exports = CONTROLLER