const mongoose = require('mongoose')
const SCHEMA = require('./schema')

const MODULE_NAME = require('path').basename(__dirname)

const MODEL = mongoose.model(MODULE_NAME, SCHEMA)

module.exports = MODEL